#!/bin/bash

# Stop on error
set -e

# Compile the Go code and create the 'main' binary
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main main.go

# Build the Docker image using the Dockerfile
docker build -t sumitsagar/ms1:v1 .

# Log in to Docker Hub
echo "Please enter your Docker Hub password:"
docker login -u sumitsagar

# Push the image to Docker Hub
docker push sumitsagar/ms1:v1

# (Optional) Run the Docker container, exposing port 8080
# Uncomment the line below if you want to run the container automatically
# docker run -p 8080:8080 ms1
