# Use a lightweight alpine image for the runtime container
FROM golang:1.16 as builder

# Set the working directory inside the container
WORKDIR /app

# Copy the compiled binary and index.html from the local machine
COPY ./main /app/main
COPY ./index.html /app/index.html

# Expose port 8080 for the app to listen on
EXPOSE 8080

# Set the working directory for the CMD instruction
WORKDIR /app

# Run the binary
CMD ["/app/main"]
