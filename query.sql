CREATE DATABASE DriverDB;

USE DriverDB;

CREATE TABLE Drivers (
    ID INT PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(50),
    ImageURL VARCHAR(255)
);

CREATE TABLE Locations (
    ID INT PRIMARY KEY AUTO_INCREMENT,
    DriverID INT,
    Latitude FLOAT,
    Longitude FLOAT,
    FOREIGN KEY (DriverID) REFERENCES Drivers(ID)
);

-- Inserting dummy data
INSERT INTO Drivers (Name, ImageURL) VALUES
('Driver1', 'https://some.image.url/driver1.jpg'),
('Driver2', 'https://some.image.url/driver2.jpg');
