package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

func getDrivers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// Define the API URL
	apiURL := "http://ms2:8081/drivers"

	// Make a GET request to the API
	resp, err := http.Get(apiURL)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	// Read the API response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Parse the JSON response
	var drivers []Driver
	err = json.Unmarshal(body, &drivers)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Send the parsed data as a JSON response
	json.NewEncoder(w).Encode(drivers)
}

func getLocation(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// Get driverID from the URL
	vars := mux.Vars(r)
	driverID := vars["driverID"]

	// Construct the API URL
	apiURL := fmt.Sprintf("http://ms2:8081/location/%s", driverID)

	// Make a GET request to the API
	resp, err := http.Get(apiURL)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	// Read the API response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Parse the JSON response
	var location Location
	err = json.Unmarshal(body, &location)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Send the parsed data as a JSON response
	json.NewEncoder(w).Encode(location)
}

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/", serveHome).Methods("GET")

	r.HandleFunc("/drivers", func(w http.ResponseWriter, r *http.Request) {
		getDrivers(w, r)
	}).Methods("GET")

	r.HandleFunc("/location/{driverID}", func(w http.ResponseWriter, r *http.Request) {
		getLocation(w, r)
	}).Methods("GET")

	http.Handle("/", r)
	fmt.Println("Server running on port 8080")
	http.ListenAndServe(":8080", nil)
}
func serveHome(w http.ResponseWriter, r *http.Request) {
	wd, _ := os.Getwd()
	fmt.Printf("Current working directory: %s\n", wd)
	fmt.Println("Serving index.html")
	http.ServeFile(w, r, "index.html")
}

type Driver struct {
	ID       string `json:"ID"`
	ImageURL string `json:"ImageURL"`
	Name     string `json:"Name"`
}

type Location struct {
	Latitude  float64 `json:"Latitude"`
	Longitude float64 `json:"Longitude"`
}
